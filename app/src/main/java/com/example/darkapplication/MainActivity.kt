package com.example.darkapplication

import android.content.SharedPreferences
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AppCompatDelegate
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var num_click = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val sh = getSharedPreferences("0",0)
        var ed : SharedPreferences.Editor

        night_switch.isChecked = onCheckNightMode()

        num.text = sh.getInt("click",0).toString()
        num_click = Integer.parseInt(num.text.toString())

        night_switch.isChecked = sh.getBoolean("night_mode",false)
        setNightMode(sh.getBoolean("night_mode",false))

        click.setOnClickListener {
            num_click++
            num.text = num_click.toString()

            ed = sh.edit()
            ed.putInt("click",num_click)
            ed.apply()
        }

        night_switch.setOnCheckedChangeListener{compoundButton, b ->

            setNightMode(b)

            ed = sh.edit()
            ed.putBoolean("night_mode",b)
            ed.apply()
        }
    }
    fun setNightMode (b : Boolean){
        if(b){
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        }
    }

    fun onCheckNightMode() : Boolean{
        return resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    }
}